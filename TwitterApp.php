<?php
require_once('TwitterAPIExchange.php');

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = array(
    'oauth_access_token' => "228010210-wCHzKN2DeJkNj47NDjIOG8gIfCusuBqJnJ2OqXTd",
    'oauth_access_token_secret' => "k5wb3LummwKIAk0ysZSw0gSrofkFxKRPpxGvMODqbawm2",
    'consumer_key' => "TiL025um3uJ4QXQflVzspKX10",
    'consumer_secret' => "C2EKhonj7Yd9bhUcPQyUbxPA6rORwLjyqVLfCRa3V2SOhy8oef"
);

$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";

$requestMethod = "GET";

$screenNames = ['dms_guild', 'Wizards_DnD', 'SageAdviceDnD', 'DnDBeyond', 'JeremyECrawford', 'mikemearls'];

$string = [];

foreach($screenNames as $screenName) {
  $getfield = '?screen_name='.$screenName.'&count=10&tweet_mode=extended';

  $twitter = new TwitterAPIExchange($settings);
  
  $string = array_merge($string, json_decode($twitter->setGetfield($getfield)
  ->buildOauth($url, $requestMethod)
  ->performRequest(),$assoc = TRUE));
}

if(array_key_exists("errors", $string)) {echo "<h3>Sorry, there was a problem.</h3><p>Twitter returned the following error message:</p><p><em>".$string[errors][0]["message"]."</em></p>";exit();}
/*echo "<pre>";
print_r($string);
echo "</pre>";*/
echo "
<html>
<head>
    <title>Les Tweets du Donjon</title>
    <meta charset='UTF-8' />
    <link rel='stylesheet' type='text/css' href='./css/theme.css' />
    <link rel='stylesheet' type='text/css' href='./css/Chart.min.css' />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.0/css/font-awesome.min.css'>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'>
</head>
<body class='container-fluid body'>
  <header class='text-center'>
    <h1>Les tweets du donjons</h1>
  </header>
  <hr class='hr' />
  <main class='row'>
    <section class='col-6'>
      <div>
        <p>Sur ce site, nous réunissons les tweets de comptes liés à Donjons et Dragons. Ce sont des annonces d'évènements,
         des explications de règles, des conseils et astuces pour les maîtres de jeu...</p>
      </div>";
$favorites = [];
$retweets = [];
$ratios = [];
foreach($string as $tweet) {
  array_push($favorites, $tweet['favorite_count']);
  array_push($retweets, $tweet['retweet_count']);
  array_push($ratios, ($tweet['favorite_count']+$tweet['retweet_count'])/2);
}
echo "
      <p>Voici une analyse de la popularité des comptes Twitter sur 'Dungeons & Dragons' sur les derniers tweets :</p>
      <script src='./js/Chart.min.js'></script>
      <div class='chart-container tweet' style='position: relative; height:50vh; width:45vw'>
        <canvas id='chart'></canvas>
      <script>
      var ctx = document.getElementById('chart');
      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: [ '1'";
            for($j = 2 ; $j < sizeof($string)-2; $j++) {
              echo ",'".$j."'";
            }
          echo "],
              datasets: [{
                  label: 'Nb de favoris',
                  data: [". implode(', ', $favorites) ."],
                  borderColor: 'rgba(255, 0, 0, 1)',
                  borderWidth: 1
              },{
                label: 'Nb de retweets',
                data: [". implode(', ', $retweets) ."],
                borderColor: 'rgba(0, 255, 0, 1)',
                borderWidth: 1
            },{
              label: 'Ratio entre favoris et retweets',
              data: [". implode(', ', $ratios) ."],
              borderColor: 'rgba(0, 0, 255, 1)',
              borderWidth: 1
          }]
          },
          options: {
            scales: {
                yAxes: [{
                    ticks: {
                      beginAtZero: true,
                      steps: 10,
                      stepValue: 5,
                      max: 800
                    }
                }]
            }
        }
      });
      </script>
      
    <footer class='text-center'>
      <p>Site créé par Shentor et Namor - 2020 - &copy; Tout droits réservés</p>
    </footer>
    </section>
    <section class='col-6' id='timeline'>";
    foreach($string as $tweet) {
      $urls = array_reverse($tweet['entities']['urls']);
      $text = $tweet['full_text'];
      for($i = 0 ; $i < count($urls) ; $i++) {
        $indiceEnd = $urls[$i]['indices'][1];
        $indiceBegin = $urls[$i]['indices'][0];
        $expandedUrl = $urls[$i]['expanded_url'];
        $text = substr($text, 0, $indiceEnd) . "</a>" . substr($text, $indiceEnd);
        $text = substr($text, 0, $indiceBegin) . "<a target='_blank' href='".$expandedUrl."'>" . substr($text, $indiceBegin);
      }
        echo "
        <div class='tweet row'>
            <figure class='col-2'>
              <img class='rounded' src='".$tweet['user']['profile_image_url']."' />
            </figure>
            <h2 class='col-10'>".$tweet['user']['name']."</h2>
            <small class='col-3'><a target='_blank' href='".$tweet['user']['entities']['url']['urls'][0]['expanded_url']."'>@".$tweet['user']['screen_name']."</a></small>
            <small class='col-3'>".date_format(date_create($tweet['created_at']), "d/m/Y H:i:s")."</small>
            <small class='col-2'><i class='icon-retweet'></i> ".$tweet['retweet_count']."</small>
            <small class='col-2'><i class='icon-heart'></i> ".$tweet['favorite_count']."</small>
            <p>".$text."</p>";
          
      if(array_key_exists('media', $tweet['entities'])) {
        $medias = $tweet['entities']['media'];
      
        foreach($medias as $media) {
          if($media['type'] == "photo") {
            echo "
            <img class='img rounded' src='".$media['media_url_https']."' heigth='".$media['sizes']['small']['h']."' width='".$media['sizes']['small']['w']."' />";
          }
          if($media['type'] == "video") {
            echo "
            <video controls src='".$media['media_url_https']."' />";
          }
        }
      }
        echo "</div>";
    }
    echo "
    </section>
  </main>
</body>
</html>";