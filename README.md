Cette application, développée en PHP, est accessible via un serveur local ou distant.
Pour lancer un serveur local, commencez par télécharger et installer WampServer http://www.wampserver.com/.

Ouvrez un terminal de commandes et rendez vous dans votre dossier de projets Wamp (C:/wamp64/www), puis clonez l’application à l’intérieur avec la commande suivante

git clone https://gitlab.com/NamorEsther/api-twitter-sous-php.git

Lancez le logiciel Wamp, et rendez vous sur l’url http://localhost/

Sélectionnez le nom du projet api-tweeter-sous-php pour lancer l’application.

Réalisé par Romain Le Rest et Gaël Bulteau